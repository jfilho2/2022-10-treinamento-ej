# Treinamento EJ - Cloud

## Steps to run

- Configure .env with MongoDB URI
- `npm install`
- `npm start`

## cURL commands examples

- Create user: `curl -d '{"name": "Limoni", "email": "limoni@mail.com"}' -H "Content-Type: application/json" -X POST http://localhost:3000/user`
- List users: `curl http://localhost:3000/user`
- Get user: `curl http://localhost:3000/user/:id`
- Patch user: `curl -d '{"name": "New name"}' -H "Content-Type: application/json" -X PATCH http://localhost:3000/user/:id`

## Docker commands

- Build: `docker build . -t user-crud-api:1.0.0`
- Run: `docker run -p 3000:3000 user-crud-api:1.0.0`

## Deploy

Set your .env file correctly, then:

- `gcloud config set project $PROJECT_NAME`
- `gcloud builds submit -t gcr.io/$PROJECT_NAME/user-crud .`
- `gcloud run deploy user-crud-api --image gcr.io/$PROJECT_NAME/user-crud:latest --platform managed --region=us-west1 --allow-unauthenticated --memory 256Mi`
